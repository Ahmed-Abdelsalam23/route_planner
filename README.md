# Route Planning Project

This repository contains code for the a Route Planning project.

![Map Image](map.png)

## Cloning

To clone this project, ensure you use the `--recurse-submodules` flag to include all the necessary submodules. Use either of the following commands depending on your preference:

- **Using HTTPS:**
  ```bash
  git clone https://gitlab.com/Ahmed-Abdelsalam23/route_planner.git --recurse-submodules
  ```

## Dependencies for Running Locally

Ensure you have the following dependencies installed to run this project locally:

- **CMake** (version 3.11.3 or higher)
  - All Operating Systems: [Installation Instructions](https://cmake.org/install/)
- **Make** (version 4.1 for Linux and Mac, 3.81 for Windows)
  - Linux: Installed by default on most distributions
  - Mac: [Install Xcode command line tools](https://developer.apple.com/xcode/features/)
  - Windows: [Installation Instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
- **GCC/G++** (version 7.4.0 or higher)
  - Linux: Installed by default on most distributions
  - Mac: Follow the same instructions as for Make - [Install Xcode command line tools](https://developer.apple.com/xcode/features/)
  - Windows: Recommended to use [MinGW](http://www.mingw.org/)
- **IO2D**
  - Follow the installation instructions [here](https://github.com/cpp-io2d/P0267_RefImpl/blob/master/BUILDING.md) for all operating systems. This library must be built in a location where CMake `find_package` will be able to find it.

## Compiling and Running

### Compiling

To compile the project:

1. Create and change to a `build` directory:
   ```bash
   mkdir build && cd build
   ```
2. Run `cmake` and `make`:
   ```bash
   cmake ..
   make
   ```

### Running

After compilation, the executable will be located in the `build` directory. Run the project with:

- **Default map:**
  ```bash
  ./OSM_A_star_search
  ```
- **Specifying a map file:**
  ```bash
  ./OSM_A_star_search -f ../<your_osm_file.osm>
  ```

## Testing

The unit tests executable will also be in the `build` directory. To run the tests, execute:

```bash
./test
```
